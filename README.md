# Cineplus

## Getting Started

This full-stack web application features the system used by the staff of a movie rental company Cineplus to handle daily business operation, such as process movies, genres inventory as well as customer rentals and returns. This is the front-end/client part of the application.

## Backend project

[Cineplus-api](https://gitlab.com/tanjiawei/cineplus-api)

## Demo: Click to use app

[![demo](demo.jpg)](https://warm-crag-20467.herokuapp.com)

### Prerequisites

You need Node.js to install the various packages used in this application. You can go to [Nodejs.org](https://nodejs.org/en/download/) to download and install node. The `npm` function will be used to install/initialize this application.

### Installing

Run below command in your terminal to install various dependencies

```
npm install
```

### Running

Run below command in your terminal to start this program

```
npm start
```

## Deployment

You can use deploy this project with Firebase. For more information on deployment on Firebase, please visit their [page](https://firebase.google.com/).

## Built With

- [React](https://reactjs.org/) - UI
- [Bootstrap](https://getbootstrap.com/) - Styles
- [Font Awesome](https://fontawesome.com/) - Components
- [Lodash](https://lodash.com/) - Movies array manipulation
- [axios](https://www.npmjs.com/package/axios) - Connect to backend
- [joi-browser](https://www.npmjs.com/package/joi-browser) - Front-end form validation
- [prop-types](https://www.npmjs.com/package/prop-types) - Verify component props type
- [raven-js](https://www.npmjs.com/package/raven-js) - Log errors remotely into Sentry account
- [react-toastify](https://www.npmjs.com/package/react-toastify) - Beautiful notifications
- [jwt-decode](https://www.npmjs.com/package/jwt-decode) - Decode JWT for URL display

## Authors

- **Westley Tan** - _Initial work_

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
