import moment from "moment";

export function parseDateOption(timeString, statement) {
  return timeString ? moment(timeString).utc().format("YYYY-MM-DD") : statement;
}

export function parseDateTime(str) {
  return str ? moment(str).utc().format("YYYY-MM-DD") : "Open";
}
