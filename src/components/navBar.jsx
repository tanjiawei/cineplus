import React from "react";
import { NavLink, Link } from "react-router-dom";

const NavBar = ({ user }) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light mb-4 logo">
      <Link className="navbar-brand" to="/movies">
        <img alt="icon" height={40} src={require("../logo.png")} />
        <span className="px-1 logo-size">CINEPLUS</span>
      </Link>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <NavLink className="nav-item nav-link" to="/movies">
            MOVIES
          </NavLink>
          <NavLink className="nav-item nav-link" to="/customers">
            CUSTOMERS
          </NavLink>
          <NavLink className="nav-item nav-link" to="/rentals">
            RENTALS
          </NavLink>
          {!user && (
            <React.Fragment>
              <NavLink className="nav-item nav-link float-to-right" to="/login">
                LOGIN
              </NavLink>
            </React.Fragment>
          )}
          {user && (
            <React.Fragment>
              <div className="float-to-right d-flex flex-row">
                <NavLink className="nav-item nav-link" to="/profile">
                  Hello, {user.name}
                </NavLink>
                <NavLink className="nav-item nav-link" to="/logout">
                  LOG OUT
                </NavLink>
              </div>
            </React.Fragment>
          )}
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
