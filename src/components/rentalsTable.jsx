import React, { Component } from "react";
import { Link } from "react-router-dom";
import Table from "./common/table";
import auth from "../services/authService";
import moment from "moment";
import { parseDateOption } from "../utils/parseDate";

class RentalTable extends Component {
  columns = [
    {
      path: "_id",
      label: "Ref",
      content: (rental) => (
        <Link to={"/rentals/" + rental._id}>{this.createRef(rental)}</Link>
      ),
    },
    { path: "customer.name", label: "Customer Name" },
    { path: "movie.title", label: "Movie Name" },
    {
      path: "dateOut",
      label: "Rent Date",
      content: (rental) => parseDateOption(rental.dateOut),
    },
    {
      path: "dateReturned",
      label: "Return Date",
      content: (rental) => parseDateOption(rental.dateReturned, "Open"),
    },
    {
      path: "rentalFee",
      label: "Total Rate",
      content: (rental) =>
        rental.dateReturned ? "$ " + rental.rentalFee : null,
    },
  ];

  returnColumn = {
    key: "return",
    label: "Status",
    content: (rental) => (
      <button
        disabled={rental.dateReturned}
        onClick={() => this.props.onReturn(rental)}
        className={
          rental.dateReturned
            ? "btn bth-sm btn-secondary"
            : "btn bth-sm btn-primary"
        }
      >
        {rental.dateReturned ? "Closed" : "Return"}
      </button>
    ),
  };

  createRef(rental) {
    return (
      rental._id.slice(20, 24) + moment(rental.dateOut).utc().format("mmss")
    );
  }

  constructor() {
    super();
    if (auth.getCurrentUser()) this.columns.push(this.returnColumn);
  }

  render() {
    const { rentals, sortColumn, onSort } = this.props;
    return (
      <Table
        sortColumn={sortColumn}
        onSort={onSort}
        data={rentals}
        columns={this.columns}
      />
    );
  }
}

export default RentalTable;
