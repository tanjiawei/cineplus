import React, { Component } from "react";
import { Link } from "react-router-dom";
import Table from "./common/table";

class RentalTable extends Component {
  columns = [
    {
      path: "_id",
      label: "Customer Ref.",
      content: (customer) => (
        <Link to={"/customers/" + customer._id}>
          {this.createRef(customer)}
        </Link>
      ),
    },
    { path: "name", label: "Customer Name" },
    { path: "phone", label: "Customer Phone" },
    {
      path: "isGold",
      label: "Membership",
      content: (customer) =>
        customer.isGold ? (
          <span className="badge badge-success">Gold Member</span>
        ) : null,
    },
  ];

  createRef(customer) {
    return customer._id.slice(16, 24);
  }

  render() {
    const { customers, sortColumn, onSort } = this.props;
    return (
      <Table
        sortColumn={sortColumn}
        onSort={onSort}
        data={customers}
        columns={this.columns}
      />
    );
  }
}

export default RentalTable;
