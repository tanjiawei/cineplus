import React from "react";
import _ from "lodash";
import Joi from "joi-browser";
import Form from "./common/form";
import { toast } from "react-toastify";
import Pagination from "./common/pagination";
import { paginate } from "../utils/paginate";
import { getCustomers } from "../services/customerService";
import CustomersTable from "./customersTable";

class Customers extends Form {
  state = {
    customers: [],
    currentPage: 1,
    pageSize: 4,
    sortColumn: { path: "isGold", order: "desc" },
    data: { status: 1, customerName: "", customerPhone: "" },
    errors: {},
  };

  schema = {
    status: Joi.number().label("Customer membership").allow(null, ""),
    customerName: Joi.string().label("Customer Name").allow(null, ""),
    customerPhone: Joi.string().label("Customer Phone").allow(null, ""),
  };

  doSubmit = async (e) => {
    e.preventDefault();
    if (Object.values(this.state.data).join("").length < 6) return null;
    try {
      const { data: customers } = await getCustomers(this.state.data);
      console.log(this.state.data);
      this.setState({ customers });
    } catch (ex) {
      if (ex.response) {
        toast.error("Something unexpected happened. Please try again later.");
      }
    }
  };

  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  handleSort = (sortColumn) => {
    this.setState({
      sortColumn,
    });
  };

  getPageData = () => {
    const {
      pageSize,
      currentPage,
      customers: allCustomers,
      sortColumn,
    } = this.state;
    const searched = allCustomers;
    const sorted = _.orderBy(searched, [sortColumn.path], [sortColumn.order]);
    const customers = paginate(sorted, currentPage, pageSize);
    // need to fix this bug
    if (customers.length === 0 && currentPage > 0) {
      this.setState({ currentPage: currentPage - 1 });
    }
    console.log(
      "Pleaes fix the fatal bug in rentals.jsx --> f: getPageData setState, which shouldn't exist here but must be executed later."
    );
    // need to fix this bug
    return { totalCount: sorted.length, data: customers };
  };

  render() {
    const { pageSize, currentPage, sortColumn } = this.state;
    const { totalCount, data: customers } = this.getPageData();
    const statusGroup = [
      { _id: 2, name: "None members" },
      { _id: 1, name: "All members" },
      { _id: 0, name: "Gold members" },
    ];
    return (
      <div className="row">
        <div className="col-3">
          <h5>Search customers</h5>
          <form onSubmit={this.doSubmit}>
            {this.renderSelect("status", "Gold Member", statusGroup)}
            {this.renderInput("customerName", "Customer Name")}
            {this.renderInput("customerPhone", "Customer Phone")}
            {this.renderButton("Search", "warning")}
          </form>
        </div>
        <div className="col">
          <p>Total {totalCount} customers records retrieved.</p>
          <CustomersTable
            customers={customers}
            sortColumn={sortColumn}
            onSort={this.handleSort}
          />
          <Pagination
            itemsCount={totalCount}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>
      </div>
    );
  }
}

export default Customers;
