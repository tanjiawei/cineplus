import React from "react";

const ListGroup = ({
  genres,
  textProperty,
  valueProperty,
  selectedGenre,
  onItemSelect,
}) => {
  return (
    <ul className="list-group">
      {genres.map((genre) => (
        <li
          style={{ cursor: "pointer" }}
          onClick={() => {
            onItemSelect(genre);
          }}
          key={genre[valueProperty]}
          className={
            genre === selectedGenre
              ? "list-group-item active"
              : "list-group-item"
          }
        >
          {genre[textProperty]}
        </li>
      ))}
    </ul>
  );
};

ListGroup.defaultProps = {
  textProperty: "name",
  valueProperty: "_id",
};

export default ListGroup;
