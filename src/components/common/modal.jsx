import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const ModalBox = ({
  color,
  launchButtonLabel,
  boxClassName,
  boxTitle,
  boxContent,
  callback,
}) => {
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color={color} onClick={toggle}>
        {launchButtonLabel}
      </Button>
      <Modal isOpen={modal} toggle={toggle} className={boxClassName}>
        <ModalHeader toggle={toggle}>{boxTitle}</ModalHeader>
        <ModalBody>{boxContent}</ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={() => alert("callback")}>
            Confirm
          </Button>
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default ModalBox;
