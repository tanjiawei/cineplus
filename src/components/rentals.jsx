import React from "react";
import _ from "lodash";
import Joi from "joi-browser";
import Form from "./common/form";
import { toast } from "react-toastify";
import { getRentals, returnMovie } from "../services/rentalService";
import RentalsTable from "./rentalsTable";
import Pagination from "./common/pagination";
import { paginate } from "../utils/paginate";

class Rentals extends Form {
  state = {
    rentals: [],
    currentPage: 1,
    pageSize: 4,
    sortColumn: { path: "dateOut", order: "asc" },
    data: { status: 2, movieName: "", customerName: "", customerPhone: "" },
    errors: {},
  };

  schema = {
    status: Joi.number().label("Status").allow(null, ""),
    movieName: Joi.string().label("Movie Name").allow(null, ""),
    customerName: Joi.string().label("Customer Name").allow(null, ""),
    customerPhone: Joi.string().label("Customer Phone").allow(null, ""),
  };

  doSubmit = async (e) => {
    e.preventDefault();
    if (Object.values(this.state.data).join("").length <= 0) return null;
    try {
      const { data: rentals } = await getRentals(this.state.data);
      console.log(this.state.data);
      this.setState({ rentals });
    } catch (ex) {
      if (ex.response) {
        toast.error("Something unexpected happened. Please try again later.");
      }
    }
  };

  handleReturn = async (rental) => {
    const rentals = [...this.state.rentals];
    const index = rentals.indexOf(rental);
    rentals[index] = { ...rental };
    try {
      const { data: rentalReturned } = await returnMovie(rental);
      rentals[index].dateReturned = rentalReturned.dateReturned;
      rentals[index].rentalFee = rentalReturned.rentalFee;
      this.setState({ rentals });
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        toast.error("This rental has already been deleted.");
      }
    }
  };

  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  handleSort = (sortColumn) => {
    this.setState({
      sortColumn,
    });
  };

  getPageData = () => {
    const {
      pageSize,
      currentPage,
      rentals: allRentals,
      sortColumn,
    } = this.state;
    const searched = allRentals;
    const sorted = _.orderBy(searched, [sortColumn.path], [sortColumn.order]);
    const rentals = paginate(sorted, currentPage, pageSize);
    // need to fix this bug
    if (rentals.length === 0 && currentPage > 0) {
      this.setState({ currentPage: currentPage - 1 });
    }
    console.log(
      "Pleaes fix the fatal bug in rentals.jsx --> f: getPageData setState, which shouldn't exist here but must be executed later."
    );
    // need to fix this bug
    return { totalCount: sorted.length, data: rentals };
  };

  render() {
    const { pageSize, currentPage, sortColumn } = this.state;
    const { totalCount, data: rentals } = this.getPageData();
    const statusGroup = [
      { _id: 2, name: "Open" },
      { _id: 1, name: "All" },
      { _id: 0, name: "Closed" },
    ];
    return (
      <div className="row">
        <div className="col-3">
          <h5>Search entries</h5>
          <form onSubmit={this.doSubmit}>
            {this.renderSelect("status", "Status", statusGroup)}
            {this.renderInput("movieName", "Movie Name")}
            {this.renderInput("customerName", "Customer Name")}
            {this.renderInput("customerPhone", "Customer Phone")}
            {this.renderButton("Search", "warning")}
          </form>
        </div>
        <div className="col">
          <p>Total {totalCount} rental entries retrieved.</p>
          <RentalsTable
            rentals={rentals}
            sortColumn={sortColumn}
            onReturn={this.handleReturn}
            onSort={this.handleSort}
          />
          <Pagination
            itemsCount={totalCount}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>
      </div>
    );
  }
}

export default Rentals;
