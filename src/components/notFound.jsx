import React from "react";
import { Link } from "react-router-dom";
import auth from "../services/authService";
import _ from "lodash";

const NotFound = () => {
  const user = auth.getCurrentUser();
  if (user)
    return (
      <div className="h2 my-5 pt-5">
        <Link className="text-decoration-none" to="/">
          Hello, {_.capitalize(user.name) || "My favourite person"}. Just a
          friendly reminder: You went to an non-existing page. Click me to
          return to main page.
        </Link>
        <h4 className="py-5">Error code: 404</h4>
      </div>
    );
  return (
    <React.Fragment>
      <h1 className="py-2">
        <i className="fa fa-exclamation-triangle text-danger mr-2" />
        We couldn't find that Web page.
      </h1>
      <h4 className="py-2">Error 404</h4>
      <h5 className="py-2">
        We're sorry you ended up here. Sometimes a page gets moved or deleted,
        but hopefully we can help you find what you're looking for. What next?
      </h5>
      <ul className="py-2">
        <li className="py-2">
          Return to the <Link to="/">home page</Link>;
        </li>
        <li>
          <a href="mailto:admin@cineplus.ca">Contact us</a> and we'll help you
          out.
        </li>
      </ul>
    </React.Fragment>
  );
};

export default NotFound;
