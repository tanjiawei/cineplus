import http from "./httpService";

const apiEndPoint = "/customers";

function customerUrl(id) {
  return `${apiEndPoint}/${id}`;
}

export function getCustomers(input) {
  input.status = Number(input.status);
  //get method won't work here, post and is already used.
  return http.put(apiEndPoint, input);
}

export function getCustomer(rentalId) {
  return http.get(customerUrl(rentalId));
}

export function addCustomer(customer) {
  return http.post(apiEndPoint, customer);
}
