import http from "./httpService";

const apiEndPoint = "/rentals";

function rentalUrl(id) {
  return `${apiEndPoint}/${id}`;
}

export function getRentals(input) {
  input.status = Number(input.status);
  //get method won't work here, post is already used.
  return http.put(apiEndPoint, input);
}

export function getRental(rentalId) {
  return http.get(rentalUrl(rentalId));
}

export function addRental(rental) {
  return http.post(apiEndPoint, rental);
}

export function returnMovie(rental) {
  return http.post("/returns", {
    rentalId: rental._id,
    customerId: rental.customer._id,
    movieId: rental.movie._id,
  });
}
