import http from "./httpService";

export function getGenres() {
  try {
    return http.get("/genres");
  } catch (err) {
    console.error("Opps!", err);
  }
}
